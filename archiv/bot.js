const newsletter = async (req, res) => {
    if (req.body.token && req.body.title && req.body.description) {
      const hash = crypto
        .createHash("sha256")
        .update(req.body.token)
        .digest("base64");
      if (hash === config.api.token) {
        if (req.body.title.length > 1 && req.body.description.length > 1) {
          await nl.post(
            client,
            DB,
            "Fridays For Future",
            "https://i.imgur.com/4BGBizI.png",
            req.body.title,
            req.body.description,
            "anderes",
            null,
            null
          );
          return res.json([true, 200, "200 OK"]);
        } else {
          return res.json([false, 404, "404 Empty Fields"]);
        }
      } else {
        return res.json([false, 403, "403 Wrong Token"]);
      }
    } else {
      return res.json([false, 404, "404 Wrong Syntax"]);
    }
  };
    // if (interaction.commandName === "Im Newsletter teilen") {
  //   const guild = client.guilds.cache.get(config.guildid);
  //   const user = guild.members.cache.get(interaction.user.id);
  //   const newsletter = require(`./archiv/newsletter_contextmenu.js`);
  //   newsletter.post(interaction, client, user, guild, DB, nl.post);
  // } else

  module.exports = {
    newsletter,
  };

  const DB = sequelize.define("newsletter", {
    user: {
      type: Sequelize.STRING,
      primaryKey: true,
      unique: true,
    },
  });

  const sequelize = new Sequelize({
    dialect: "sqlite",
    logging: false,
    storage: "database.sqlite",
  });
  const crypto = require("crypto");
  const Sequelize = require("sequelize");
  const nl = require("./archiv/newsletter_systems.js");
  const { DAVClient } = require("tsdav");
  
  const calender = new DAVClient({
    serverUrl: config.dav.url,
    credentials: {
      username: config.dav.username,
      password: config.dav.password,
    },
    authMethod: "Basic",
    defaultAccountType: "caldav",
  });

  await calender.login();
  await DB.sync();