const {
  SlashCommandBuilder,
  SlashCommandSubcommandBuilder,
  WebhookClient,
  EmbedBuilder,
} = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("calender")
    .addSubcommand(
      new SlashCommandSubcommandBuilder()
        .setName("add")
        .setDescription("Kalender » Erstelle ein neues Event!")
        .addStringOption((option) =>
          option
            .setName("beschreibung")
            .setDescription(
              "Event Erstellen » Um Was geht es bei dem Event?"
            )
            .setRequired(true)
        )
        .addStringOption((option) =>
          option
            .setName("type")
            .setDescription(
              "Event Erstellen » Was ist das für ein Event? (Streik, Kundgebung, Orga Call, AG Call, etc...)"
            )
            .setRequired(true)
        )
        .addStringOption((option) =>
          option
            .setName("ort")
            .setDescription("Wo wird das Event sein?")
            .setRequired(true)
        )
        .addStringOption((option) =>
          option
            .setName("uhrzeit")
            .setDescription(
              "Um wie viel Uhr das Event Beginnt (z.b. '12:00' [ohne die '])"
            )
            .setRequired(true)
        )
        .addIntegerOption((option) =>
          option
            .setName("tag")
            .setDescription("Der Tag an dem der Streik ist? (1-31)")
            .setRequired(true)
        )
        .addStringOption((option) =>
          option
            .setName("monat")
            .setDescription("Der Monat in dem das Event ist?")
            .setRequired(true)
            .addChoices(
              { name: "Januar", value: "1" },
              { name: "Februar", value: "2" },
              { name: "März", value: "3" },
              { name: "April", value: "4" },
              { name: "Mai", value: "5" },
              { name: "Juni", value: "6" },
              { name: "Juli", value: "7" },
              { name: "August", value: "8" },
              { name: "September", value: "9" },
              { name: "Oktober", value: "10" },
              { name: "November", value: "11" },
              { name: "Dezember", value: "12" }
            )
        )
    )
    .setDescription("« Kalender »"),
  async execute(interaction) {
    if (interaction.options.getSubcommand() === "add") {
      const beschreibung = await interaction.options.getString("beschreibung");
      const type = await interaction.options.getString("type");
      const ort = await interaction.options.getString("ort");
      const uhrzeit = await interaction.options.getString("uhrzeit");
      const tag = await interaction.options.getInteger("tag");
      const monat = await interaction.options.getString("monat");
      const webhookClient = new WebhookClient({ url: 'https://discord.com/api/webhooks/976883968307785768/ZhQSUqbG1c0dDiXlBMJLrYZPkxKU_ep4UH8azUhhNKnP3nTfVbR90eI36EY-n0kK8o4y' });
      const Embed = new EmbedBuilder()
        .setColor("#2f3137")
        .setTitle("Neuer Kalender Eintrag")
        .addFields([
          {
            name: 'Beschreibung', value: beschreibung
          },
          {
            name: 'Art', value: type
          },
          {
            name: 'Ort', value: ort
          },
          {
            name: 'Datum', value: `${tag}.${monat}. | ${uhrzeit}`
          },
        ])
        .setTimestamp()
        .setFooter(
          {
            text: "Fridays For Future Graz",
            iconURL: "https://i.imgur.com/t8pvWVS.png"
          }
        );

      await webhookClient.send({
        content: '<@893577594837041192>',
        username: interaction.user.tag,
        avatarURL: `https://cdn.discordapp.com/avatars/${interaction.user.id}/${interaction.user.avatar}.png`,
        embeds: [Embed],
      });
      await interaction.reply({
        content: `<:ok_hand_FFF:834355803640037416> **»** Der Eintrag wurde gespeichert und wir nächsten Sonntag ausgesendet!\nSolltest du etwas falsches angegeben haben, melde dich umgehend bei der AG IT!`,
        ephemeral: true,
      });
    }
  },
};
