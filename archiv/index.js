const express = require("express");
const routes = require("./routes/route");

const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use("/", routes);

const listener = app.listen(process.env.PORT || 1337, () => {
  console.log("API ist Online auf Port " + listener.address().port);
});
