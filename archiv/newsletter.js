const {
  ActionRowBuilder,
  ButtonBuilder,
  EmbedBuilder,
  SlashCommandBuilder,
  SlashCommandSubcommandGroupBuilder,
  SlashCommandSubcommandBuilder,
} = require("discord.js");

const wait = require("util").promisify(setTimeout);

module.exports = {
  data: new SlashCommandBuilder()
    .setName("newsletter")
    .addSubcommandGroup(
      new SlashCommandSubcommandGroupBuilder()
        .setName("post")
        .setDescription("Newsletter » Poste etwas in den Newsletter.")
        .addSubcommand(
          new SlashCommandSubcommandBuilder()
            .setName("streik")
            .setDescription("Newsletter » Poste einen Streik in den Newsletter")
            .addStringOption((option) =>
              option
                .setName("titel")
                .setDescription("Was soll der Titel des Beitrags sein?")
                .setRequired(true)
            )
            .addStringOption((option) =>
              option
                .setName("beschreibung")
                .setDescription("Was soll die Beschreibung des Beitrags sein?")
                .setRequired(true)
            )
            .addStringOption((option) =>
              option
                .setName("ort")
                .setDescription("Wo wird der Streik sein?")
                .setRequired(true)
            )
            .addStringOption((option) =>
              option
                .setName("uhrzeit")
                .setDescription(
                  "Um wie viel Uhr der Streik Startet (z.b. '12:00' [ohne die '])"
                )
                .setRequired(true)
            )
            .addIntegerOption((option) =>
              option
                .setName("tag")
                .setDescription("Der Tag an dem der Streik ist? (1-31)")
                .setRequired(true)
            )
            .addStringOption((option) =>
              option
                .setName("monat")
                .setDescription("Der Monat in dem der Streik ist?")
                .setRequired(true)
                .addChoices(
                  { name: "Januar", value: "1" },
                  { name: "Februar", value: "2" },
                  { name: "März", value: "3" },
                  { name: "April", value: "4" },
                  { name: "Mai", value: "5" },
                  { name: "Juni", value: "6" },
                  { name: "Juli", value: "7" },
                  { name: "August", value: "8" },
                  { name: "September", value: "9" },
                  { name: "Oktober", value: "10" },
                  { name: "November", value: "11" },
                  { name: "Dezember", value: "12" }
                )
            )
            .addStringOption((option) =>
              option
                .setName("jahr")
                .setDescription(
                  "Das Jahr in dem der Streik ist? (Optional [Nur wenn es erst im Nächsten Jahr ist!])"
                )
                .addChoices(
                  { name: "2023", value: "2023" },
                  { name: "2024", value: "2024" }
                )
            )
        )
        .addSubcommand(
          new SlashCommandSubcommandBuilder()
            .setName("anderes")
            .setDescription(
              "Newsletter » Poste etwas anderes in den Newsletter"
            )
            .addStringOption((option) =>
              option
                .setName("titel")
                .setDescription("Was soll der Titel des Beitrags sein?")
                .setRequired(true)
            )
            .addStringOption((option) =>
              option
                .setName("beschreibung")
                .setDescription("Was soll die Beschreibung des Beitrags sein?")
                .setRequired(true)
            )
        )
    )
    .addSubcommand(
      new SlashCommandSubcommandBuilder()
        .setName("msg")
        .setDescription(
          "Newsletter » Sende die Nachricht mit der man den Newsletter abonnieren kann."
        )
    )
    .addSubcommand(
      new SlashCommandSubcommandBuilder()
        .setName("subscribers")
        .setDescription(
          "Newsletter » Schau dir alle Abonnenten des Newsletters an."
        )
    )
    .setDescription("Team » Staff"),
  async execute(interaction, client, post) {
    if (interaction.options.getSubcommand() === "msg") {
      await interaction.reply({
        content: "Der Embed wurde Gesendet!",
        ephemeral: true,
      });
      const Button1 = new ActionRowBuilder().addComponents([
        new ButtonBuilder()
          .setLabel("Newsletter Abonnieren")
          .setCustomId("newsletter_start")
          .setStyle("SUCCESS")
          .setEmoji("📫")
      ]);
      const Button2 = new ActionRowBuilder().addComponents([
        new ButtonBuilder()
          .setLabel("Newsletter Deabonnieren")
          .setCustomId("newsletter_stop")
          .setStyle("DANGER")
          .setEmoji("📪")
      ]);
      const Embed = new EmbedBuilder()
        .setColor("#2f3137")
        .setTitle("Newsletters For Future")
        .setDescription(
          "Abonniere jetzt unseren Newsletter und erhalte als einer der ersten Infos zu den Neusten Streiks, Aktionen und anderes direkt in deine Discord Direktnachrichten!"
        )
        .setFooter(
          "Fridays For Future Graz",
          "https://i.imgur.com/t8pvWVS.png"
        );
      await interaction.channel.send({
        embeds: [Embed],
        components: [Button1, Button2],
      });
    } else if (interaction.options.getSubcommand() === "streik") {
      await interaction.deferReply({
        content: "<a:loading:910481358508421160>",
        ephemeral: true,
      });
      const titel = interaction.options.getString("titel");
      const beschreibung = interaction.options.getString("beschreibung");
      const rawzeit = interaction.options.getString("uhrzeit");
      const uhrzeit = await rawzeit.split(":");
      const tag = interaction.options.getInteger("tag");
      const monat = interaction.options.getString("monat");
      const ort = interaction.options.getString("ort");
      const jahr = interaction.options.getString("jahr");
      const date = new Date();
      if (jahr !== null) {
        date.setMinutes(uhrzeit[1]);
        date.setHours(uhrzeit[0]);
        date.setDate(tag);
        date.setMonth(monat);
        date.setFullYear(jahr);
      } else {
        date.setMinutes(uhrzeit[1]);
        date.setHours(uhrzeit[0]);
        date.setDate(tag);
        date.setMonth(monat);
        date.setFullYear(date.getFullYear());
      }
      if (uhrzeit[1] !== undefined) {
        await post(
          client,
          DB,
          interaction.user.tag,
          "https://cdn.discordapp.com/avatars/" +
          interaction.user.id +
          "/" +
          interaction.user.avatar +
          ".webp",
          titel,
          beschreibung,
          "streik",
          date,
          ort
        );
        await wait(1000);
        await interaction.editReply({
          content:
            "<:ok_hand_FFF:834355803640037416> **»** Alle Abonnenten des Newsletters haben den Beitrag erhalten!",
          ephemeral: true,
        });
      } else {
        await wait(500);
        await interaction.editReply({
          content:
            "🤔 **»** Die ausgewählte Uhrzeit ist Falsch! (Format: Stunden:Minuten [z.b. 12:00])",
          ephemeral: true,
        });
      }
    } else if (interaction.options.getSubcommand() === "anderes") {
      await interaction.deferReply({
        content: "<a:loading:910481358508421160>",
        ephemeral: true,
      });
      const titel = interaction.options.getString("titel");
      const beschreibung = interaction.options.getString("beschreibung");
      await post(
        client,
        DB,
        interaction.user.tag,
        "https://cdn.discordapp.com/avatars/" +
        interaction.user.id +
        "/" +
        interaction.user.avatar +
        ".webp",
        titel,
        beschreibung,
        "anderes",
        null,
        null
      );
      await wait(1000);
      await interaction.editReply({
        content:
          "<:ok_hand_FFF:834355803640037416> **»** Alle Abonnenten des Newsletters haben den Beitrag erhalten!",
        ephemeral: true,
      });
    } else if (interaction.options.getSubcommand() === "subscribers") {
      await interaction.deferReply({
        content: "<a:loading:910481358508421160>",
        ephemeral: true,
      });
      const guild = client.guilds.cache.get("776180755126157312");
      const users = await DB.findAll();
      let userlist = "";
      let requests = users.map(async (user) => {
        // eslint-disable-next-line no-async-promise-executor
        return new Promise(async (cb) => {
          const member = await guild.members.fetch(user.dataValues.user);
          console.log(member.user.tag);
          userlist = userlist.concat(
            " **•** **Name:** `" +
            member.user.tag +
            "` **×** **User:** <@" +
            member.user.id +
            "> **×** **ID:** `" +
            member.user.id +
            "`\n"
          );
          cb();
        });
      });

      Promise.all(requests).then(async () => {
        let Embed;
        if (users.length >= 1) {
          Embed = new EmbedBuilder()
            .setColor("#2f3137")
            .setTitle("Newsletter Abonnenten")
            .addFields([
              {
                name: 'Anzahl', value: users.length + " Abonnenten"
              },
            ])
            .setDescription(userlist)
            .setFooter(
              "Fridays For Future Graz",
              "https://i.imgur.com/t8pvWVS.png"
            );
        } else {
          Embed = new EmbedBuilder()
            .setColor("#2f3137")
            .setTitle("Newsletter Abonnenten")
            .addFields([
              {
                name: 'Abonnenten', value: "0 Abonnenten"
              },
            ])
            .setDescription("Keine Abonnenten")
            .setFooter(
              "Fridays For Future Graz",
              "https://i.imgur.com/t8pvWVS.png"
            );
        }
        await interaction.editReply({
          embeds: [Embed],
        });
      });
    }
  },
};
