const { ActionRowBuilder, EmbedBuilder, ButtonBuilder } = require("discord.js");
const wait = require("util").promisify(setTimeout);
const Button1 = new ActionRowBuilder().addComponents([
  new ButtonBuilder()
    .setLabel("Newsletter Deabonnieren")
    .setCustomId("newsletter_stop")
    .setStyle("DANGER")
    .setEmoji("📪")
]);
const Button2 = new ActionRowBuilder().addComponents([
  new ButtonBuilder()
    .setLabel("Newsletter Abonnieren")
    .setCustomId("newsletter_start")
    .setStyle("SUCCESS")
    .setEmoji("📫")
]);
const Embed1 = new EmbedBuilder()
  .setColor("#2f3137")
  .setTitle("👋 Willkommen!")
  .setDescription(
    "Willkommen in unserem Discord Newsletter!\nAb jetzt erhälst du aktuellste Infos zu Streiks oder anderen Aktionen direkt in deine Discord Direktnachrichten!"
  )
  .setFooter("Newsletters For Future", "https://i.imgur.com/t8pvWVS.png");

const Embed2 = new EmbedBuilder()
  .setColor("#2f3137")
  .setTitle("🥲 Tschüß!")
  .setDescription(
    "Willkommen in unserem Discord Newsletter!\nAb jetzt erhälst du aktuellste Infos zu Streiks oder anderen Aktionen direkt in deine Discord Direktnachrichten!"
  )
  .setFooter("Newsletters For Future", "https://i.imgur.com/t8pvWVS.png");

module.exports = {
  start: async (interaction, client, DB) => {
    // await interaction.reply({
    //   content:
    //     "<:error:939603328990580896> **»** Wir arbeiten zurzeit am Bot! Bitte warte 10-15 Minuten und versuche es erneut!",
    //   ephemeral: true,
    // });
    DB.sync().then(function () {
      DB.findOrCreate({
        where: {
          user: interaction.user.id,
        },
        defaults: {
          user: interaction.user.id,
        },
      }).then(async function (result) {
        const created = result[1];
        if (created) {
          await interaction.reply({
            content:
              "<:ok_hand_FFF:834355803640037416> **»** Du bist nun in unserem Newsletter angemeldet und bekommst alles neuste nun Direkt in deine Direktnachrichten!",
            ephemeral: true,
          });
          await wait(2500);
          await interaction.user
            .send({
              embeds: [Embed1],
              components: [Button1],
            })
            .catch(async () => {
              await DB.destroy({
                where: {
                  user: interaction.user.id,
                },
              });
              await interaction.editReply({
                content:
                  "🥲 **»** Leider konnte ich dir keine DM senden also wurdest du wieder vom Newsletter entfernt! Aktiviere deine Direktnachrichten und versuche es erneut!",
                ephemeral: true,
              });
            });
        } else {
          await interaction.reply({
            content:
              "🤪 **»** Du Dummerchen... Du bist doch hast unseren Newsletter doch bereits abonniert!",
            ephemeral: true,
          });
        }
      });
    });
  },
  stop: async (interaction, client, DB) => {
    // await interaction.reply({
    //   content:
    //     "<:error:939603328990580896> **»** Wir arbeiten zurzeit am Bot! Bitte warte 10-15 Minuten und versuche es erneut!",
    //   ephemeral: true,
    // });
    DB.sync().then(function () {
      DB.destroy({
        where: {
          user: interaction.user.id,
        },
      }).then(async function (result) {
        const created = result;
        if (created) {
          await interaction.reply({
            content:
              "<:ok_hand_FFF:834355803640037416> **»** Du bist nun von unserem Newsletter abgemeldet und bekommst nun keine Direktnachrichten mehr!",
            ephemeral: true,
          });
          await wait(2500);
          await interaction.user.send({
            embeds: [Embed2],
            components: [Button2],
          });
        } else {
          await interaction.reply({
            content:
              "🤪 **»** Du Dummerchen... Du hast unseren Newsletter doch garnicht abonniert!",
            ephemeral: true,
          });
        }
      });
    });
  },
};
