const wait = require("util").promisify(setTimeout);

module.exports = {
  post: async (interaction, client, userr, guild, DB, post) => {
    await interaction.deferReply({
      content: "<a:loading:910481358508421160>",
      ephemeral: true,
    });
    if (userr.roles.cache.some((role) => role.id === "781180726124609566")) {
      const channel = await guild.channels.cache.get(interaction.channelId);
      const message = await channel.messages.cache.get(interaction.targetId);
      const beschreibung = await message.content.toString();
      await post(
        client,
        DB,
        interaction.user.tag,
        "https://cdn.discordapp.com/avatars/" +
          interaction.user.id +
          "/" +
          interaction.user.avatar +
          ".webp",
        "Neuer Post",
        beschreibung,
        "anderes",
        null,
        null
      );

      await wait(1000);
      await interaction.editReply({
        content:
          "<:ok_hand_FFF:834355803640037416> **»** Alle Abonnenten des Newsletters haben den Beitrag erhalten!",
        ephemeral: true,
      });
    } else {
      await interaction.editReply({
        content:
          "🥲 **»** Um das zu tun brauchst du die Rolle <@&781180726124609566>",
        ephemeral: true,
      });
    }
  },
};
