const { EmbedBuilder } = require("discord.js");

module.exports = {
  post: async (
    client,
    DB,
    author,
    avatar,
    title,
    description,
    type,
    date,
    place
  ) => {
    const guild = client.guilds.cache.get("776180755126157312");
    let Embed;
    if (type === "streik") {
      let minutes = "" + date.getMinutes();
      if (minutes.length === 1) {
        minutes = "0" + minutes;
      }
      Embed = new EmbedBuilder()
        .setColor("#2f3137")
        .setAuthor({
          name: author,
          iconURL: avatar,
        })
        .setTitle("📨 " + title)
        .setDescription(description)
        .addField(
          "🗓️ Wann?",
          "Am " +
            date.getDate() +
            "." +
            date.getMonth() +
            "." +
            date.getFullYear() +
            " um " +
            date.getHours() +
            ":" +
            minutes
        )
        .addField("🧭 Wo?", place)
        .setFooter("Newsletters For Future", "https://i.imgur.com/t8pvWVS.png");
    } else {
      Embed = new EmbedBuilder()
        .setColor("#2f3137")
        .setAuthor({
          name: author,
          iconURL: avatar,
        })
        .setTitle("📨 " + title)
        .setDescription(description)
        .setFooter("Newsletters For Future", "https://i.imgur.com/t8pvWVS.png");
    }
    DB.sync().then(function () {
      DB.findAll().then(async function (result) {
        await result.every(async (user) => {
          const member = await guild.members.cache.get(user.dataValues.user);
          if (member) {
            await member.send({ embeds: [Embed] }).catch(async () => {
              await DB.destroy({
                where: {
                  user: user.dataValues.user,
                },
              });
            });
          }
        });
        return true;
      });
    });
  },
};
