module.exports = {
    toggle: async (interaction) => {
        const member = await interaction.guild.members.cache.get(interaction.user.id);
        const role = await interaction.guild.roles.cache.get('978663220942348349');
        if (!member._roles.includes("978663220942348349")) {
            await member.roles.add(role);
            await interaction.reply({ content: "**»** Ich habe dir Zugriff auf die Kategorie gegeben!", ephemeral: true });
        } else if (member._roles.includes("978663220942348349")) {
            await member.roles.remove(role);
            await interaction.reply({ content: "**»** Ich habe dir Zugriff auf die Kategorie entfernt!", ephemeral: true });
        }
    },
};
