const {
  SlashCommandBuilder,
  SlashCommandSubcommandBuilder,
  ActionRowBuilder,
  ModalBuilder,
  TextInputBuilder,
  TextInputStyle,
  WebhookClient,
} = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("drive")
    .addSubcommand(
      new SlashCommandSubcommandBuilder()
        .setName("activate")
        .setDescription("DriveForFuture » Beantrage eine Aktivierung deines Drive Accounts!")
    )
    .setDescription("DriveForFuture"),
  async execute(interaction) {
    if (interaction.options.getSubcommand() === "activate") {
      const webhookClient = new WebhookClient({ url: 'https://discord.com/api/webhooks/976581290591653958/SDAz0YSBtQxVxmo6DW17jGkBAeWAz_URpY1V-MRr57HLSBLE8HaVGLpjV3wHED9X9fYC' });
      const modal = new ModalBuilder()
        .setCustomId('activateModal')
        .setTitle('Aktivieren des Drive Accounts');

      const emailInput = new TextInputBuilder()
        .setCustomId('email')
        .setLabel('Wie lautet deine DriveForFuture E-Mail?')
        .setStyle(TextInputStyle.Short);

      const ActionRow = new ActionRowBuilder().addComponents([emailInput]);

      modal.addComponents([ActionRow]);
      await interaction.showModal(modal);

      const filter = (interaction) => interaction.customId === 'activateModal';
      const interaction2 = await interaction.awaitModalSubmit({ filter, time: 480_000 });
      const email = await interaction2.fields.getTextInputValue('email');
      await webhookClient.send({
        content: `<@893577594837041192> **»** Massiiii bitte aktiviere meinen Account mit der E-Mail "\`${email}\`"!`,
        username: interaction.user.tag,
        avatarURL: `https://cdn.discordapp.com/avatars/${interaction.user.id}/${interaction.user.avatar}.png`,
      });      
      await interaction2.reply({
        content: `<:ok_hand_FFF:834355803640037416> **»** Dein Drive Account mit der E-Mail "\`${email}\`" wird so schnell wie möglich aktiviert!`,
        ephemeral: true,
      });
    }
  },
};
