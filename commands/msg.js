const {
  SlashCommandBuilder,
  SlashCommandSubcommandBuilder,
} = require("discord.js");

module.exports = {
  data: new SlashCommandBuilder()
    .setName("msg")
    .addSubcommand(
      new SlashCommandSubcommandBuilder()
        .setName("send")
        .setDescription("Messages » Sende eine Nachricht.")
        .addStringOption((option) =>
          option
            .setName("content")
            .setDescription(
              "Nachricht Senden » Was soll in der Nachricht stehen?"
            )
            .setRequired(true)
        )
    )
    .addSubcommand(
      new SlashCommandSubcommandBuilder()
        .setName("copy")
        .setDescription("Messages » Kopiere eine Nachricht.")
        .addStringOption((option) =>
          option
            .setName("id")
            .setDescription(
              "Nachricht Kopieren » Welche Nachricht willst du kopieren? (Nachrichten-ID)"
            )
            .setRequired(true)
        )
    )
    .addSubcommand(
      new SlashCommandSubcommandBuilder()
        .setName("edit")
        .setDescription("Messages » Editiere eine Nachricht.")
        .addStringOption((option) =>
          option
            .setName("id")
            .setDescription(
              "Nachricht Editieren » Welche Nachricht willst du editieren? (Nachrichten-ID)"
            )
            .setRequired(true)
        )
        .addStringOption((option) =>
          option
            .setName("content")
            .setDescription(
              "Nachricht Editieren » Was soll in der Nachricht stehen?"
            )
            .setRequired(true)
        )
    )
    .addSubcommand(
      new SlashCommandSubcommandBuilder()
        .setName("delete")
        .setDescription("Messages » Lösche eine Nachricht.")
        .addStringOption((option) =>
          option
            .setName("id")
            .setDescription(
              "Nachricht Löschen » Welche Nachricht willst du löschen? (Nachrichten-ID)"
            )
            .setRequired(true)
        )
        .addBooleanOption((option) =>
          option
            .setName("bestätigung")
            .setDescription(
              "Nachricht Löschen » Willst du die Nachricht wirklich Löschen?"
            )
            .setRequired(true)
        )
    )
    .setDescription("Messages"),
  async execute(interaction) {
    if (interaction.options.getSubcommand() === "send") {
      const content = await interaction.options.getString("content");
      const content2 = await content.split(`/n`).join("\n");
      const msg = await interaction.channel.send({ content: `${content2}` });
      await interaction.reply({
        content: `<:ok_hand_FFF:834355803640037416> **»** Die Nachricht wurde gesendet!\nDie ID ist: \`${msg.id}\`!\nhttps://discord.com/channels/${interaction.guild.id}/${interaction.channel.id}/${msg.id}`,
        ephemeral: true,
      });
    } else if (interaction.options.getSubcommand() === "copy") {
      const id = await interaction.options.getString("id");
      const oldmsg = await interaction.channel.messages
        .fetch(id)
        .catch(async () => {
          await interaction.reply({
            content: `<:error:939603328990580896> **»** Die Nachricht mit der ID \`${id}\` konnte nicht gefunden werden!`,
            ephemeral: true,
          });
        });
      const newmsg = await interaction.channel.send(oldmsg.content);
      await interaction.reply({
        content: `<:ok_hand_FFF:834355803640037416> **»** Die Nachricht mit der ID \`${id}\` wurde kopiert!\nhttps://discord.com/channels/${interaction.guild.id}/${interaction.channel.id}/${oldmsg.id}\nhttps://discord.com/channels/${interaction.guild.id}/${interaction.channel.id}/${newmsg.id}`,
        ephemeral: true,
      });
    } else if (interaction.options.getSubcommand() === "edit") {
      const id = await interaction.options.getString("id");
      const content = await interaction.options.getString("content");
      const content2 = await content.split(`/n`).join("\n");
      const oldmsg = await interaction.channel.messages
        .fetch(id)
        .catch(async () => {
          await interaction.reply({
            content: `<:error:939603328990580896> **»** Die Nachricht mit der ID \`${id}\` konnte nicht gefunden werden!`,
            ephemeral: true,
          });
        });
      const newmsg = await oldmsg.edit(`${content2}`);
      await interaction.reply({
        content: `<:ok_hand_FFF:834355803640037416> **»** Die Nachricht mit der ID \`${id}\` wurde editiert!\nhttps://discord.com/channels/${interaction.guild.id}/${interaction.channel.id}/${newmsg.id}`,
        ephemeral: true,
      });
    } else if (interaction.options.getSubcommand() === "delete") {
      const id = await interaction.options.getString("id");
      const bestätigung = await interaction.options.getBoolean("bestätigung");
      if (bestätigung) {
        const msg = await interaction.channel.messages
          .fetch(id)
          .catch(async () => {
            await interaction.reply({
              content: `<:error:939603328990580896> **»** Die Nachricht mit der ID \`${id}\` konnte nicht gefunden werden!`,
              ephemeral: true,
            });
          });
        await msg.delete();
        await interaction.reply({
          content: `<:ok_hand_FFF:834355803640037416> **»** Die Nachricht mit der ID \`${id}\` wurde gelöscht!`,
          ephemeral: true,
        });
      } else {
        await interaction.reply({
          content: `<:error:939603328990580896> **»** Der Löschvorgang wurde abgebrochen da Option \`bestätigung\` auf \`false\` ist!`,
          ephemeral: true,
        });
      }
    }
  },
};
