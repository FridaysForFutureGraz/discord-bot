module.exports = {
  copy: async (interaction) => {
    const member = await interaction.guild.members.cache.get(interaction.user.id);
    if (member.roles.cache.some((role) => role.id === "781180726124609566")) {
      const oldmsg = await interaction.channel.messages
        .fetch(interaction.targetId)
        .catch(async () => {
          await interaction.reply({
            content: `<:error:939603328990580896> **»** Die Nachricht mit der ID \`${interaction.targetId}\` konnte nicht gefunden werden!`,
            ephemeral: true,
          });
        });
      const newmsg = await interaction.channel.send(oldmsg.content);
      await interaction.reply({
        content: `<:ok_hand_FFF:834355803640037416> **»** Die Nachricht mit der ID \`${interaction.targetId}\` wurde kopiert!\nhttps://discord.com/channels/${interaction.guild.id}/${interaction.channel.id}/${oldmsg.id}\nhttps://discord.com/channels/${interaction.guild.id}/${interaction.channel.id}/${newmsg.id}`,
        ephemeral: true,
      });
    } else {
      await interaction.editReply({
        content:
          "🥲 **»** Um das zu tun brauchst du die Rolle <@&781180726124609566>",
        ephemeral: true,
      });
    }
  },
};
