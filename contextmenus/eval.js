const {
  EmbedBuilder,
  ButtonBuilder,
  ActionRowBuilder,
} = require("discord.js");

module.exports = {
  eval: async (interaction, client) => {
    const guild = client.guilds.cache.get("776180755126157312");
    const user = guild.members.cache.get(interaction.user.id);
    function clean(text) {
      if (typeof text === "string")
        return text
          .replace(/`/g, "`" + String.fromCharCode(8203))
          .replace(/@/g, "@" + String.fromCharCode(8203));
      else return text;
    }
    function short(str, num) {
      if (str.length <= num) {
        return str;
      }
      return str.slice(0, num) + "...";
    }
    await interaction.deferReply({ ephemeral: true });
    if (user.roles.cache.some((role) => role.id === "781180726124609566")) {
      const channel = await client.channels.cache.get(interaction.channelId);
      await channel.messages
        .fetch(interaction.targetId)
        .then(async (message) => {
          let code = await message.content
            .replace("```js", "")
            .replace("```", "");
          try {
            let evaled = await eval(code);

            if (typeof evaled !== "string")
              evaled = await require("util").inspect(evaled);
            const Embed = new EmbedBuilder()
              .setColor("#2f3137")
              .setTitle("Eval")
              .addFields([
                { name: "Input", value: "```js\n" + short(code, 950) + "\n```" },
                { name: "Output", value: "```js\n" + short(clean(evaled), 950) + "\n```" },
              ])
              .setFooter({
                text: "Fridays For Future Graz",
                iconURL: "https://i.imgur.com/t8pvWVS.png"
              });
            await interaction.editReply({ embeds: [Embed], ephemeral: true });
          } catch (err) {
            let error = err;
            if (typeof error !== "string")
              error = await require("util").inspect(err);
            const Embed = new EmbedBuilder()
              .setColor("#2f3137")
              .setTitle("Eval")
              .addFields([
                { name: "Input", value: "```js\n" + short(code, 950) + "\n```" },
                { name: "Output", value: "```js\n" + short(error, 950) + "\n```" },
              ])
              .setFooter({
                text: "Fridays For Future Graz",
                iconURL: "https://i.imgur.com/t8pvWVS.png"
              });
            await interaction.editReply({ embeds: [Embed], ephemeral: true });
          }
        });
    } else {
      await interaction.editReply({
        content:
          "🥲 **»** Um das zu tun brauchst du die Rolle <@&781180726124609566>",
        ephemeral: true,
      });
    }
  },
};
