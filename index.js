/* eslint-disable no-empty */
const config = require("./config.json");
const fs = require("fs");
const { Client, IntentsBitField, Collection } = require("discord.js");
const client = new Client({ intents: new IntentsBitField(131071) });

client.commands = new Collection();
client.once("ready", async () => {
  // await client.guilds.cache.get(config.guildid).members.fetch().then(collection => {
  //   collection.forEach(member => {
  //     fs.appendFileSync("mems.jsonc", "[" + member + ',"' + member.user.tag + '"],\n');
  //   });
  // });

  // console.log(
  //   await client.api
  //     .applications(client.user.id)
  //     .guilds(config.guildid)
  //     .commands.get()
  // );

  console.log(`Eingeloggt als ${client.user.tag}!`);
  client.user.setPresence({
    activities: [
      {
        name: "FutureOnAir",
        type: 2,
      },
    ],
    status: "dnd",
  });
});

const commandFiles = fs
  .readdirSync("./commands")
  .filter((file) => file.endsWith(".js"));

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  client.commands.set(command.data.name, command);
}

// Check for Command Interactions
client.on("interactionCreate", async (interaction) => {
  if (!interaction.isCommand()) return;
  const command = client.commands.get(interaction.commandName);

  if (!command) return;

  try {
    await command.execute(interaction, client);
  } catch (error) {
    console.log(error);
    await interaction.reply({
      content:
        "<:error:939603328990580896> **»** Es gab einen Fehler beim Ausführen des Commands!",
      ephemeral: true,
    });
  }
});

// Check for Context Menu Interactions
client.on("interactionCreate", async (interaction) => {
  if (interaction.commandName === "Code Ausführen") {
    const evalo = require(`./contextmenus/eval.js`);
    evalo.eval(interaction, client);
  } else if (interaction.commandName === "Nachricht Kopieren") {
    const copy = require(`./contextmenus/copy.js`);
    copy.copy(interaction);
  }
});

// Check for Button Interactions
client.on("interactionCreate", async (interaction) => {
  if (!interaction.isButton()) return;
  if (interaction.customId === "fridayslink_toggle") {
    const fridayslink = require(`./buttons/fridayslink.js`);
    fridayslink.toggle(interaction, client);
  }
});

// Check for Select Menu Interactions
client.on("interactionCreate", async (interaction) => {
  if (!interaction.isSelectMenu()) return;
  const guild = client.guilds.cache.get(config.guildid);
  const user = await guild.members.cache.get(interaction.user.id);
  let roles;
  switch (interaction.customId) {
    case "rollen":
      roles = [
        "814481200181542962",
        "781936828475244634",
        "1030950227714244698",
        "781936618986537000",
        "814477883216232458",
        "781937582828683265",
        "781936425666871327",
        "989244321985331230",
        "898156847154995302",
        "832997988355407932",
        "930951873605795850",
        "832998856472002572",
        "1030952115234951198",
      ];
  }

  switch (interaction.customId) {
    default:
      roles.forEach((roleid) => {
        let role = guild.roles.cache.find((r) => r.id === roleid);
        if (interaction.values.includes(roleid)) {
          if (!user.roles.cache.some((role) => role.id === roleid)) {
            user.roles.add(role);
          }
        } else {
          if (user.roles.cache.some((role) => role.id === roleid)) {
            user.roles.remove(role);
          }
        }
      });
      interaction.reply({
        content: "Deine Rollen wurden Geupdated!",
        ephemeral: true,
      });
  }
});


const { App } = require("@slack/bolt");

const app = new App({
  signingSecret: config.slack.signingSecret,
  token: config.slack.token,
  socketMode: true,
  appToken: config.slack.appToken,
});

(async () => {
  await app.start(5637 || 3000);
  console.log("Slack Bot Gestartet!");
})();

function short(str, num) {
  if (str.length <= num) {
    return str;
  }
  return str.slice(0, num) + "...";
}

app.event("message", async ({ message }) => {
  config.fridayslink.every(async (ids) => {
    if (message.channel === ids.slack) {
      try {
        const guild = await client.guilds.cache.get(config.guildid);
        const channel = await guild.channels.cache.get(ids.discord);
        const webhooks = await channel.fetchWebhooks();
        const webhook = webhooks.first();
        if (message.subtype === 'message_changed') {
          const user = await app.client.users.info({
            user: message.message.user,
          });
          let pinged;
          let content = message.message.text;
          const ping = await content.substring(
            content.indexOf("<@") + 2,
            content.lastIndexOf(">")
          );
          if (content.includes("<@U")) {
            pinged = await app.client.users.info({
              user: ping,
            });

            content = await content.replace(
              "<@" + ping + ">",
              "@" + pinged.user.real_name
            );
          }

          if (message.message.thread_ts) {
            let thread = await channel.threads.cache.find(x => x.name === message.message.thread_ts);
            if (thread) {
              const thch = await guild.channels.cache.get(thread.id);
              const dcmsg = await thch.messages.cache.find(m => m.content === message.previous_message.text);
              if (dcmsg) {
                if (dcmsg.author.username === `🔗 ${user.user.real_name}`) {
                  await webhook.editMessage(dcmsg.id, {
                    content: short(content, 950),
                    threadId: thread.id
                  });
                }
              }
            }
          } else {
            const dcmsg = await channel.messages.cache.find(m => m.content === message.previous_message.text);
            if (dcmsg) {
              if (dcmsg.author.username === `🔗 ${user.user.real_name}`) {
                await webhook.editMessage(dcmsg.id, {
                  content: content
                });
              }
            }
          }
        } else {
          const user = await app.client.users.info({
            user: message.user,
          });
          let pinged;
          let content = message.text;
          const ping = await content.substring(
            content.indexOf("<@") + 2,
            content.lastIndexOf(">")
          );
          if (content.includes("<@U")) {
            pinged = await app.client.users.info({
              user: ping,
            });

            content = await content.replace(
              "<@" + ping + ">",
              "@" + pinged.user.real_name
            );
          }

          if (message.thread_ts) {
            let thread = await channel.threads.cache.find(x => x.name === message.thread_ts);
            if (thread) {
              await webhook.send({
                content: short(content, 950),
                username: `🔗 ${user.user.real_name}`,
                avatarURL: user.user.profile.image_original,
                threadId: thread.id
              })
            } else {
              const msg = await app.client.conversations.history({
                channel: ids.slack,
                latest: message.ts,
                limit: 1
              });
              let user2;
              try {
                user2 = await app.client.users.info({
                  user: msg.messages[0].user,
                });
              }
              catch { }
              if (!user2) {
                thread = await channel.threads.create({
                  name: message.thread_ts,
                  autoArchiveDuration: 10080,
                  reason: 'Fridayslink',
                });
                await webhook.send({
                  content: `Jemand schrieb:\n>>> \`${msg.messages[0].text}\``,
                  username: `🌐 Fridayslink » Threads`,
                  avatarURL: 'https://media.discordapp.net/attachments/776180755126157316/978699522593140836/Logo_Dark.png',
                  threadId: thread.id
                });
                await webhook.send({
                  content: short(content, 950),
                  username: `🔗 ${user.user.real_name}`,
                  avatarURL: user.user.profile.image_original,
                  threadId: thread.id
                });
              } else {
                const dcmsg = await channel.messages.cache.find(m => m.content === msg.messages[0].text);
                if (dcmsg) {
                  if (dcmsg.author.username === `🔗 ${user2.user.real_name}`) {
                    thread = await dcmsg.startThread({
                      name: message.thread_ts,
                      autoArchiveDuration: 10080,
                      reason: 'Fridayslink',
                    });
                    await webhook.send({
                      content: short(content, 950),
                      username: `🔗 ${user.user.real_name}`,
                      avatarURL: user.user.profile.image_original,
                      threadId: thread.id
                    });
                  }
                } else {
                  thread = await channel.threads.create({
                    name: message.thread_ts,
                    autoArchiveDuration: 10080,
                    reason: 'Fridayslink',
                  });
                  await webhook.send({
                    content: `${user.user.real_name} schrieb:\n>>> \`${msg.messages[0].text}\``,
                    username: `🌐 Fridayslink » Threads`,
                    avatarURL: user.user.profile.image_original,
                    threadId: thread.id
                  });
                  await webhook.send({
                    content: short(content, 950),
                    username: `🔗 ${user.user.real_name}`,
                    avatarURL: user.user.profile.image_original,
                    threadId: thread.id
                  });
                }
              }

            }
          } else {
            await webhook.send({
              content: short(content, 950),
              username: `🔗 ${user.user.real_name}`,
              avatarURL: user.user.profile.image_original
            })

          }
        }
      } catch (err) {
        console.log(err);
      }
    }
  });
});

client.on("messageCreate", async (message) => {
  if (message.author.bot) return;
  config.fridayslink.every(async (ids) => {
    if (message.channel.id === ids.discord || message.guild.channels.cache.get(message.channelId).parentId === ids.discord) {
      try {
        const guild = await client.guilds.cache.get(config.guildid);
        let pinged;
        const member = await guild.members.cache.get(message.author.id);
        let content = message.content;
        const ping = await message.content.substring(
          content.indexOf("<@") + 2,
          content.lastIndexOf(">")
        );
        let memname = member.user.username;
        if (member.nickname) {
          memname = member.nickname;
        }
        if (content.includes("<@")) {
          pinged = await guild.members.cache.get(ping);
          if (pinged) {
            let pingname = pinged.user.username;
            if (pinged.nickname) {
              pingname = pinged.nickname;
            }
            content = await content.replace("<@" + ping + ">", "@" + pingname);
          }
        }
        let thread = message.guild.channels.cache.get(message.channelId);
        if (thread.parentId === ids.discord) {
          try {
            await app.client.chat.postMessage({
              channel: ids.slack,
              username: `🔗 ${memname}`,
              icon_url: `https://cdn.discordapp.com/avatars/${member.user.id}/${member.user.avatar}.png`,
              text: content,
              thread_ts: thread.name,
            });
          }
          catch {
            await app.client.chat.postMessage({
              channel: ids.slack,
              username: `🔗 ${memname}`,
              icon_url: `https://cdn.discordapp.com/avatars/${member.user.id}/${member.user.avatar}.png`,
              text: content,
            });
          }
        } else {
          await app.client.chat.postMessage({
            channel: ids.slack,
            username: `🔗 ${memname}`,
            icon_url: `https://cdn.discordapp.com/avatars/${member.user.id}/${member.user.avatar}.png`,
            text: content,
          });
        }

      } catch (err) {
        console.log(err);
      }
    }
  });

});

client.login(config.token);
