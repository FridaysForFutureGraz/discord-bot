const { REST } = require("@discordjs/rest");
const { Routes } = require("discord-api-types/v9");
const { token, userid, guildid } = require("./config.json");
const fs = require("fs");
const commands = [];
const commandFiles = fs
  .readdirSync("./commands")
  .filter((file) => file.endsWith(".js"));

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  commands.push(command.data.toJSON());
}

const context1 = {
  name: "Im Newsletter teilen",
  type: 3,
};
const context2 = {
  name: "Code Ausführen",
  type: 3,
};
const context3 = {
  name: "Nachricht Kopieren",
  type: 3,
};
commands.push(context1);
commands.push(context2);
commands.push(context3);

const rest = new REST({ version: "9" }).setToken(token);

(async () => {
  try {
    console.log("Slash Commands werden Reloaded!");
    await rest.put(Routes.applicationGuildCommands(userid, guildid), {
      body: commands,
    });
    console.log("Slash Commands Reloaded!");
          
  } catch (error) {
    console.error(error);
  }
})();